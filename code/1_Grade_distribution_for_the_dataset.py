# -*- coding: utf-8 -*-
"""
Created on Sat Dec 26 18:37:06 2020

@author: Thomas Chapalain
"""
import numpy as np
import matplotlib.pyplot as plt
import json

plt.close('all')

######### Loading the MoonBoard 2017 Json file  & formating the data ##########

with open('Moon17.json') as MoonBoulders:
  jsonDict = json.load(MoonBoulders)
  
Data = jsonDict['Data']
Boulders = list()
for data in Data:
    boulder = dict()
    boulder['Grade'], listMoves, boulder['Angle'] = data['Grade'], data['Moves'], data['Relief']
    holds = list()
    for move in listMoves:
        Xcoordinate, Ycoordinate, isStart, isEnd = move['X'], move['Y'], move['IsStart'], move['IsEnd']
        if isStart:
            holds.append((Xcoordinate, Ycoordinate, 'S'))
        elif isEnd:
            holds.append((Xcoordinate, Ycoordinate, 'E'))
        else :
            holds.append((Xcoordinate, Ycoordinate, 'M'))
    boulder['Holds'] = holds
    Boulders.append(boulder)
    
''' Boulder : { 'Grade' : (str), 'Angle' = (str), 'holds' :  coordinates (x,y) of the holds 
                composing the boulder and a specifier of Start/End/Middle : S/E/M (list) }'''

################ A few statistics about the boulders Dataset ##################
nBoulder = len(Boulders)

##### Distribution of the grades throughtout the Dataset - Raw
GradesRaw = dict()
for boulder in Boulders:
    grade = boulder['Grade']
    if grade not in GradesRaw.keys():
        GradesRaw[grade] = 1
    else:
        GradesRaw[grade] += 1
        
listGradesRaw = list(GradesRaw.keys()); nGrades = len(listGradesRaw); listGradesRaw.sort() # alphanumeric order
countGradesRaw = list()
for grade in listGradesRaw:
    countGradesRaw.append(GradesRaw[grade])
  
plt.figure()
plt.title('Distribution of the grades in the Dataset - nBoulder = {}'.format(nBoulder))
plt.bar([i for i in range(nGrades)], countGradesRaw, tick_label=listGradesRaw)
plt.show()

##### Score of each hold in the 18x11 matrix composing the MoonBoard 2017
nX, nY = 11, 18
dictGrades = {}
for grade in listGradesRaw:
    dictGrades[grade] = 0
    
holdsMatrix = [[dictGrades.copy() for i in range(nY)] for i in range(nX)]
for boulder in Boulders:
    holds = boulder['Holds']
    for hold in holds:
        x, y = hold[0]-1, hold[1]-1
        holdsMatrix[x][y][boulder['Grade']] += 1

def meanGrade(hold):
    ''' dict.values() et dict.keys() ont été ordonnées (ordre alphanumériques) cf dictGrades '''
    i = 1; n = 0; score = 0
    for value in list(hold.values()):
        score += i*value; n += value
        i += 1
    return list(hold.keys())[int(score/n)-1]

meanGradeDict = dictGrades.copy()
for i in range(nX):
    for j in range(nY):
        mean = meanGrade(holdsMatrix[i][j])
        meanGradeDict[mean] += 1

countMeanGrades = list(meanGradeDict.values()) # fonctionne vu que le dict.keys() est ordonné alphanumériquement

plt.figure()
plt.title('Distribution of the hold mean grade - MoonBoard 2017')
plt.bar([i for i in range(nGrades)], countMeanGrades, tick_label=listGradesRaw)
plt.show()

def weightedMeanGrade(hold):
    i = 1; score = 0; W = 0
    for grade in list(hold.keys()):
        nG = GradesRaw[grade]; ng = hold[grade]; weight = ng/nG; W += weight
        score += i*weight
        i += 1
    return list(hold.keys())[int(score/W)-1]

weightedMeanGradeDict = dictGrades.copy()
for i in range(nX):
    for j in range(nY):
        weightedMean = weightedMeanGrade(holdsMatrix[i][j])
        weightedMeanGradeDict[weightedMean] += 1

countWeightedMeanGrades = list(weightedMeanGradeDict.values()) # fonctionne vu que le dict.keys() est ordonné alphanumériquement

plt.figure()
plt.title('Distribution of the hold weighted mean grade - Moon 2017')
plt.bar([i for i in range(nGrades)], countWeightedMeanGrades, tick_label=listGradesRaw)
plt.show()

def medianGrade(hold):
    n = sum(list(hold.values())); score = 0; i = 0
    for value in list(hold.values()):
        score += value; i += 1
        if score > int(n/2):
            break
    return listGradesRaw[i-1]

medianGradeDict = dictGrades.copy()
for i in range(nX):
    for j in range(nY):
        median = medianGrade(holdsMatrix[i][j])
        medianGradeDict[median] += 1

countMedianGrades = list(medianGradeDict.values()) # fonctionne vu que le dict.keys() est ordonné alphanumériquement

plt.figure()
plt.title('Distribution of the hold median grade - MoonBoard 2017')
plt.bar([i for i in range(nGrades)], countMedianGrades, tick_label=listGradesRaw)
plt.show()

def fourthQuartileGrade(hold):
    n = sum(list(hold.values())); score = 0; i = 0
    for value in list(hold.values()):
        score += value; i += 1
        if score > int(3*n/4):
            break
    return listGradesRaw[i-1]

fourthQuartileGradeDict = dictGrades.copy()
for i in range(nX):
    for j in range(nY):
        fourthQuartile = fourthQuartileGrade(holdsMatrix[i][j])
        fourthQuartileGradeDict[fourthQuartile] += 1

countfourthQuartileGrades = list(fourthQuartileGradeDict.values()) # fonctionne vu que le dict.keys() est ordonné alphanumériquement

plt.figure()
plt.title('Distribution of the hold 4th Quartile grade - Moon 2017')
plt.bar([i for i in range(nGrades)], countfourthQuartileGrades, tick_label=listGradesRaw)
plt.show()

##### Visualisation of the spatial distribution of the Difficulty per hold
rpz = np.zeros((nX,nY))
for i in range(nX):
    for j in range(nY):
        meanGrade = weightedMeanGrade(holdsMatrix[i][j])
        for k in range(nGrades):
            if meanGrade <= listGradesRaw[k]:
                break
        rpz[i][j] = k
    
plt.figure()
plt.title('Mean difficulty per hold - HeatMap', pad=8.)
plt.imshow(rpz.T, origin='lower')
plt.xticks([i for i in range(11)],['A','B','C','D','E','F','G','H','I','J','K'])
plt.yticks([i for i in range(0,18)], [i for i in range(1,19)])
plt.colorbar()

##### Frequency of use per hold in the 18x11 matrix composing the MoonBoard 2017
''' A l'air assez pertinent : représente l'inverse de la "difficulty per hold" '''
freqUse = np.zeros((nX,nY)); nUseTot = 0
for i in range(nX):
    for j in range(nY):
        nUse = sum(list((holdsMatrix[i][j].values())))
        nUseTot +=  nUse
        freqUse[i][j] = nUse

freqUse = freqUse*100/nUseTot
    
plt.figure()
plt.title('Frequency of use per hold - HeatMap', pad=8.)
plt.imshow(freqUse.T, origin='lower')
plt.xticks([i for i in range(11)],['A','B','C','D','E','F','G','H','I','J','K'])
plt.yticks([i for i in range(0,18)], [i for i in range(1,19)])
plt.colorbar()


##### Distribution of the grades throughtout the Dataset - Associated by Difficulty

# listGrades = ['5+', '6A/+', '6B/+', '6C/+', '7A', '7A+', '7B', '7B+', '7C', '7C+', '8A/+', '8B/+']
# nGrades = len(listGrades)

# GradesRaw = dict()
# for boulder in Boulders:
#     grade = boulder['Grade']
#     if grade not in GradesRaw.keys():
#         GradesRaw[grade] = 0
#     else:
#         GradesRaw[grade] += 1

# listGradesRaw = list(GradesRaw.keys()); listGradesRaw.sort() # alphanumeric order
# countGrades = [0]*nGrades
# for grade in listGradesRaw:
#     for i in range(nGrades):
#         if grade <= listGrades[i]:
#             break
#     countGrades[i] += GradesRaw[grade]
    
# GradesAssociated = {}; i=0
# for grade in listGrades:
#     GradesAssociated[grade] = countGrades[i]
#     i+=1
    
# plt.figure()
# plt.title('Distribution of the grades associated by difficulty - nBoulder = {}'.format(nBoulder))
# plt.bar([i for i in range(nGrades)], countGrades, tick_label=listGrades)
# plt.show()

# ##### Score of each hold in the 18x11 matrix composing the MoonBoard 2017
# nX, nY = 11, 18
# dictGrades = {}
# for grade in listGrades:
#     dictGrades[grade] = 0
    
# holdsMatrix = [[dictGrades.copy() for i in range(nY)] for i in range(nX)]
# for boulder in Boulders:
#     holds = boulder['Holds']
#     for hold in holds:
#         x, y = hold[0]-1, hold[1]-1
#         for i in range(nGrades):
#             if boulder['Grade'] <= listGrades[i]:
#                 break
#         holdsMatrix[x][y][listGrades[i]] += 1

# ''' Le descripteur de "Difficulty per hold" le plus pertinent trouvé ! '''
# def weightedMeanGrade(hold):
#     i = 1; score = 0; W = 0
#     for grade in list(hold.keys()):
#         nG = GradesAssociated[grade]; ng = hold[grade]; weight = ng/nG; W += weight
#         score += i*weight
#         i += 1
#     return list(hold.keys())[int(score/W)-1]

# weightedMeanGradeDict = dictGrades.copy()
# for i in range(nX):
#     for j in range(nY):
#         weightedMean = weightedMeanGrade(holdsMatrix[i][j])
#         weightedMeanGradeDict[weightedMean] += 1

# countWeightedMeanGrades = list(weightedMeanGradeDict.values()) # fonctionne vu que le dict.keys() est ordonné alphanumériquement

# plt.figure()
# plt.title('Distribution of the hold weighted mean grade - Moon 2017')
# plt.bar([i for i in range(nGrades)], countWeightedMeanGrades, tick_label=listGrades)
# plt.show()

# ##### Visualisation of the spatial distribution of the Difficulty per hold
# rpz = np.zeros((nX,nY))
# for i in range(nX):
#     for j in range(nY):
#         meanGrade = weightedMeanGrade(holdsMatrix[i][j])
#         for k in range(nGrades):
#             if meanGrade <= listGrades[k]:
#                 break
#         rpz[i][j] = k
    
# plt.figure()
# plt.title('Mean difficulty per hold - HeatMap', pad=8.)
# plt.imshow(rpz.T, origin='lower')
# plt.xticks([i for i in range(11)],['A','B','C','D','E','F','G','H','I','J','K'])
# plt.yticks([i for i in range(0,18)], [i for i in range(1,19)])
# plt.colorbar()

        
