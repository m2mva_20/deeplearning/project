# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 15:08:53 2021

@author: Thomas Chapalain
"""

import json

import numpy as np
from numpy import random as rd

import matplotlib.pyplot as plt

plt.figure() # only fix I found to bypass the error stemming from torch/pyplot incompatibility
plt.close('all')

import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F


from BoulderDatasetPreProcessing import DatasetPreProcessing

######### Loading the MoonBoard 2017 Json file  & formating the data ##########

with open('Moon17.json') as MoonBoulders:
  jsonDict = json.load(MoonBoulders)
  
Data = jsonDict['Data']
Boulders = list()
for data in Data:
    boulder = dict()
    boulder['Grade'], listMoves, boulder['Angle'] = data['Grade'], data['Moves'], data['Relief']
    holds = list()
    for move in listMoves:
        Xcoordinate, Ycoordinate, isStart, isEnd = move['X'], move['Y'], move['IsStart'], move['IsEnd']
        if isStart:
            holds.append((Xcoordinate, Ycoordinate, 'S'))
        elif isEnd:
            holds.append((Xcoordinate, Ycoordinate, 'E'))
        else :
            holds.append((Xcoordinate, Ycoordinate, 'M'))
    boulder['Holds'] = holds
    Boulders.append(boulder)
    
''' Boulder : { 'Grade' : (str), 'Angle' = (str), 'holds' :  coordinates (x,y) of the holds 
                composing the boulder and a specifier of Start/End/Middle : S/E/M (list) }'''
    
MoonBoardDataset = DatasetPreProcessing(Boulders)
Boulders = MoonBoardDataset.preProcessedDataset
nGrades = MoonBoardDataset.nGrades

class customNN(nn.Module): 
    def __init__(self):
        super().__init__()
        ## useful layers for the dense NN
        self.inp = nn.Linear(4,5)
        self.hid1 = nn.Linear(5,7)
        self.hid2 = nn.Linear(7,15)
        self.hid3 = nn.Linear(15,20)
        self.hid4 = nn.Linear(20,20)
        self.hid5 = nn.Linear(20,30)
        self.hid6 = nn.Linear(30,20)
        self.hid7 = nn.Linear(20,13)
        self.hid8 = nn.Linear(13,6)
        self.hid9 = nn.Linear(6,4)
        self.out = nn.Linear(4,1)
        
        self.weigthPRELU = torch.Tensor([0.25]).float()

    def forward(self, x):
        ## dense NN with relu activation for assessing the difficuty of the boulder as a sum of each move difficuly
        h1 = F.prelu(self.inp(x), self.weigthPRELU)
        h2 = F.prelu(self.hid1(h1), self.weigthPRELU)
        h3 = F.relu(self.hid2(h2))
        h4 = F.prelu(self.hid3(h3), self.weigthPRELU)
        h5 = F.prelu(self.hid4(h4), self.weigthPRELU)
        h6 = F.relu(self.hid5(h5))
        h7 = F.prelu(self.hid6(h6), self.weigthPRELU)
        h8 = F.prelu(self.hid7(h7), self.weigthPRELU)
        h9 = F.relu(self.hid8(h8))
        h10 = F.prelu(self.hid9(h9), self.weigthPRELU)
        y = self.out(h10)
        return y.mean().view(1)


def trainModel(num_epochs, batch_size, criterion, optimizer, model, dataset, suffle=False):
    ''' Since each boulder has a variable number of Moves, we can't use the pytorch method 
        for loading mini-batch of the dataset which is incompatible with variable size input '''
    
    nDataset = len(dataset) 
    nBatches = int(nDataset/batch_size)
        
    train_error = []

    model.train()
    for epoch in range(num_epochs):
        epoch_average_loss = 0.0
        
        if suffle:
            rd.shuffle(dataset)
        
        ## mini_batch training for each epoch
        for n in range(nBatches):
            
            batch_loss = torch.Tensor([0.0]).float()
            for k in range(batch_size):
                x_data , y_real = dataset[n*batch_size + k]
                ## reshape of the input so that the custom NN computes sequentially the moves one after another with forward()
                x = x_data.clone()
                len_input = (x.shape)[0]
                x = x.view(len_input, 1, 4) 
                
                ## forward compute the parameters m(x), log std(x)² of the gaussian distribution representing the likelihood p(x|y)
                y_pre = model(x)

                batch_loss += criterion(y_real.float(), y_pre)
                
            batch_loss /= batch_size
            ## in order to make the mini-batch training 'manually', the gradient is only computed after batch_size inputs
            optimizer.zero_grad()
            batch_loss.backward()   
            with torch.no_grad():
                ## mandatory in order not to screw up the the computational DAG of autograd
                for param in model.parameters():
                    param.grad /= batch_size  
            ## update of the model's parameter using after completing the entire batch 
            optimizer.step()
            epoch_average_loss += batch_loss.item() * batch_size / nDataset
            
        train_error.append(epoch_average_loss)
    

        print('Epoch [{}/{}], Loss: {:.4f}'
                  .format(epoch+1, num_epochs, epoch_average_loss))
    return train_error

# Calculate the accuracy to evaluate the model
def accuracy(dataset, nClass, model):
    
    predictions = list()
    real_class = list()
    
    model.eval()
    with torch.no_grad():
        correct = 0
        for x_data, y_real in dataset:
            x = x_data.clone()
            len_input = (x.shape)[0]
            x = x.view(len_input, 1, 4) 
            y_pre = torch.round(model(x))
            
            predictions.append(y_pre)
            real_class.append(y_real)
            
            if y_pre == y_real:
                correct += 1
            
    print('Accuracy of the model : {:.2f} %'.format(100*correct / len(dataset)))
    return predictions, real_class

model = customNN().to('cpu')

''' Loss function to compute the discrepancy between model and observation '''
loss_fn = nn.MSELoss(reduction='mean') ## method s.t. args = y_obs, y_model


''' Stochastic Gradient Descent (SDG) used to update the Parameters '''
# optimizer = optim.SGD(model.parameters(), lr=0.01, momentum=0.4) 
optimizer = optim.Adam(model.parameters(), lr=0.01)
# optimizer = optim.RMSprop(model.parameters(), lr=0.01) 

# trainDataset = Boulders[:24000]
# balancedSubDataset = MoonBoardDataset.balancedProcessedSubDataset(trainDataset, 1000)
# weightsForImbalancedDataset = MoonBoardDataset.showGradesDistribution(balancedSubDataset)
subDataset = Boulders[:100]
weightsForImbalancedDataset = MoonBoardDataset.showGradesDistribution(subDataset)

num_epochs = 300
batch_size = 5


train_error = trainModel(num_epochs, batch_size, loss_fn, optimizer, model, subDataset, suffle=True)

predictions, real_class = accuracy(subDataset, nGrades, model)
    
# plot the training error wrt. the number of epochs:
plt.figure()
plt.plot(range(1, num_epochs+1), train_error)
plt.xlabel("num_epochs")
plt.ylabel("Train error")
plt.title("Visualization of convergence")

# plot the comparaison of the predicted grade with the real one
plt.figure()
plt.plot(real_class, predictions, '.')
plt.plot([i for i in range(nGrades)], [i for i in range(nGrades)], 'r--')
plt.xlabel("real grade")
plt.ylabel("predicted grade")
plt.title("Comparaison of the predicted grade and the real one")
