# -*- coding: utf-8 -*-
"""
Created on Sat Jan 23 10:25:21 2021

@author: Thomas Chapalain
"""

import json

import numpy as np
from numpy import random as rd

import matplotlib.pyplot as plt

plt.figure() # only fix I found to bypass the error stemming from torch/pyplot incompatibility
plt.close('all')

import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F


from BoulderDatasetPreProcessing_v2 import DatasetPreProcessing

######### Loading the MoonBoard 2017 Json file  & formating the data ##########

with open('Moon17.json') as MoonBoulders:
  jsonDict = json.load(MoonBoulders)
  
Data = jsonDict['Data']
Boulders = list()
for data in Data:
    boulder = dict()
    boulder['Grade'], listMoves, boulder['Angle'] = data['Grade'], data['Moves'], data['Relief']
    holds = list()
    for move in listMoves:
        Xcoordinate, Ycoordinate, isStart, isEnd = move['X'], move['Y'], move['IsStart'], move['IsEnd']
        if isStart:
            holds.append((Xcoordinate, Ycoordinate, 'S'))
        elif isEnd:
            holds.append((Xcoordinate, Ycoordinate, 'E'))
        else :
            holds.append((Xcoordinate, Ycoordinate, 'M'))
    boulder['Holds'] = holds
    Boulders.append(boulder)
    
''' Boulder : { 'Grade' : (str), 'Angle' = (str), 'holds' :  coordinates (x,y) of the holds 
                composing the boulder and a specifier of Start/End/Middle : S/E/M (list) }'''
    
MoonBoardDataset = DatasetPreProcessing(Boulders)
nGrades = MoonBoardDataset.nGrades
Boulders25 = MoonBoardDataset.preProcessedDataset25
Boulders40 = MoonBoardDataset.preProcessedDataset40

class customNN(nn.Module): 
    def __init__(self, nClass):
        super().__init__()
        ## useful layers for parallel network that will approximate the score function relative to each features
        self.in_dist = nn.Linear(2,3)
        self.hid1_dist = nn.Linear(3,5)
        self.hid2_dist = nn.Linear(5,10)
        self.hid3_dist = nn.Linear(10,10)
        self.hid4_dist = nn.Linear(10,5)
        self.hid5_dist = nn.Linear(5,3)
        self.out_dist = nn.Linear(3,1)
        
        self.in_assym = nn.Linear(2,3)
        self.hid1_assym = nn.Linear(3,5)
        self.hid2_assym = nn.Linear(5,10)
        self.hid3_assym = nn.Linear(10,5)
        self.hid4_assym = nn.Linear(5,3)
        self.out_assym = nn.Linear(3,1)
        
        self.weigthPRELU = torch.Tensor([0.25]).float()
        
        ## useful layers for mixing all the features together
        self.in_mix = nn.Linear(3,3)
        self.hid1_mix = nn.Linear(3,2)
        self.hid2_mix = nn.Linear(2,2)
        self.out_mix = nn.Linear(2,1)
        
        ## useful layers for demixing all the features together
        self.in_demix = nn.Linear(1,3)
        self.hid_demix = nn.Linear(6,8)
        self.out_demix = nn.Linear(8,10)

        ## useful layers for the classification network that will infer m(x), log std(x)² from the cumulative score of each feature
        self.out = nn.Linear(10, nClass)
        
    def forward(self, score_hold, distance, assymetry):
        ## approximation of the score function of each feature
        
        assymetryG = torch.stack((score_hold, assymetry), dim=1)
        distanceG = torch.stack((score_hold, distance), dim=1)
        # print('distG :', distanceG)
        
        h1_assymG = F.prelu(self.in_assym(assymetryG), self.weigthPRELU)
        h2_assymG = F.prelu(self.hid1_assym(h1_assymG), self.weigthPRELU)
        h3_assymG = F.prelu(self.hid2_assym(h2_assymG), self.weigthPRELU)
        h4_assymG = F.prelu(self.hid3_assym(h3_assymG), self.weigthPRELU)
        h5_assymG = F.prelu(self.hid4_assym(h4_assymG), self.weigthPRELU)
        assymG = F.prelu(self.out_assym(h5_assymG), self.weigthPRELU).view(1,-1)
        # print('assym :', assymG)

        h1_distG = F.prelu(self.in_dist(distanceG), self.weigthPRELU)
        h2_distG = F.prelu(self.hid1_dist(h1_distG), self.weigthPRELU)
        h3_distG = F.prelu(self.hid2_dist(h2_distG), self.weigthPRELU)
        h4_distG = F.prelu(self.hid3_dist(h3_distG), self.weigthPRELU)
        h5_distG = F.prelu(self.hid4_dist(h4_distG), self.weigthPRELU)
        h6_distG = F.prelu(self.hid5_dist(h5_distG), self.weigthPRELU)
        distG = F.prelu(self.out_dist(h6_distG), self.weigthPRELU).view(1,-1)
        # print('dist :', dist)
        
        mix_features = torch.stack((distG.view(-1), assymG.view(-1), score_hold), dim=1)
        # print(mix_features.shape)
        h1_mix = F.prelu(self.in_mix(mix_features), self.weigthPRELU)
        h2_mix = F.prelu(self.hid1_mix(h1_mix), self.weigthPRELU)
        h3_mix = F.prelu(self.hid2_mix(h2_mix), self.weigthPRELU)
        mix_out = F.prelu(self.out_mix(h3_mix), self.weigthPRELU)
        
        mix_out_mean = mix_out.mean().view(1)
        
        h1_demix = F.prelu(self.in_demix(mix_out_mean), self.weigthPRELU)
        h1_skipped = torch.cat((h1_demix, torch.Tensor([mix_features[:,0].mean(), mix_features[:,1].mean(), mix_features[:,2].mean()])), dim=0)

        h2_demix = F.prelu(self.hid_demix(h1_skipped), self.weigthPRELU)
        demix_out = F.prelu(self.out_demix(h2_demix), self.weigthPRELU)
        return self.out(demix_out)

def trainModel(num_epochs, batch_size, criterion, optimizer, model, dataset, suffle=False):
    ''' Since each boulder has a variable number of Moves, we can't use the pytorch method 
        for loading mini-batch of the dataset which is incompatible with variable size input '''
    
    nDataset = len(dataset) 
    nBatches = int(nDataset/batch_size)
        
    train_error = []
    

    model.train()
    for epoch in range(num_epochs):
        epoch_average_loss = 0.0
        
        if suffle:
            rd.shuffle(dataset)
        
        ## mini_batch training for each epoch
        for n in range(nBatches):
            
            batch_loss = torch.Tensor([0.]).float()
            for k in range(batch_size):
                x_data , y_real, steepness = dataset[n*batch_size + k]
                ## reshape of the input so that the custom NN computes sequentially the moves one after another with forward()
                score_hold  = x_data[:,0]
                distance    = x_data[:,1]
                assymetry   = x_data[:,2]

                ## forward compute
                y_pre = model(score_hold, distance, assymetry)

                
                ## one hot encoding of y_real
                # one_hot_encoding_y_real = torch.FloatTensor(nGrades)
                # one_hot_encoding_y_real.zero_()
                # one_hot_encoding_y_real[int(y_real.item())] = 1.
                
                # batch_loss += criterion(one_hot_encoding_y_real, y_pre)
                batch_loss += criterion(input=y_pre.view(1,-1), target=y_real)
            
            batch_loss /= batch_size
            ## in order to make the mini-batch training 'manually', the gradient is only computed after batch_size inputs
            optimizer.zero_grad()
            batch_loss.backward()   
            with torch.no_grad():
                ## mandatory in order not to screw up the the computational DAG of autograd
                for param in model.parameters():
                    # print(param.grad)
                    param.grad /= batch_size  
            ## update of the model's parameter using after completing the entire batch 
            optimizer.step()
            epoch_average_loss += batch_loss.item() * batch_size / nDataset
            
        train_error.append(epoch_average_loss)
    

        print('Epoch [{}/{}], Loss: {:.4f}'
                  .format(epoch+1, num_epochs, epoch_average_loss))
    return train_error

def accuracy(dataset, nClass, model):
    
    predictions = list()
    real_class = list()
    
    model.eval()
    with torch.no_grad():
        correct = 0
        for x_data, y_real, steepness in dataset:
            score_hold  = x_data[:,0]
            distance    = x_data[:,1]
            assymetry   = x_data[:,2]
            
            y_pre = torch.argmax(model(score_hold, distance, assymetry))
            
            predictions.append(y_pre)
            real_class.append(y_real)
            
            if y_pre == y_real:
                correct += 1
            
    print('Accuracy of the model : {:.2f} %'.format(100*correct / len(dataset)))
    return predictions, real_class

model = customNN(nGrades).to('cpu')

# rd.shuffle(Boulders)
trainDataset = Boulders40[:5000]  
weightsForImbalancedDataset = MoonBoardDataset.showGradesDistribution(trainDataset)

# trainDataset = Boulders[:24000]
# balancedSubDataset = MoonBoardDataset.balancedProcessedSubDataset(trainDataset, 110)
# weightsForImbalancedDataset = MoonBoardDataset.showGradesDistribution(balancedSubDataset)

''' Loss function to compute the discrepancy between model and observation '''
# loss_fn = nn.MSELoss(reduction='mean') ## method s.t. args = y_obs, y_model
loss_fn = nn.CrossEntropyLoss(weight=weightsForImbalancedDataset)

''' Stochastic Gradient Descent (SDG) used to update the Parameters '''
# optimizer = optim.SGD(model.parameters(), lr=0.01) 
optimizer = optim.Adam(model.parameters(), lr=0.01)
# optimizer = optim.RMSprop(model.parameters(), lr=0.01, momentum=0.55) 

num_epochs = 50
batch_size = 10


train_error = trainModel(num_epochs, batch_size, loss_fn, optimizer, model, trainDataset, suffle=True)

predictions, real_class = accuracy(trainDataset, nGrades, model)
testDataset = Boulders40[5000:]
rd.shuffle(testDataset)
subTestDataset = testDataset[:5000]
predictions, real_class = accuracy(subTestDataset, nGrades, model)
    
# plot the training error wrt. the number of epochs:
plt.figure()
plt.plot(range(1, num_epochs+1), train_error)
plt.xlabel("num_epochs")
plt.ylabel("Train error")
plt.title("Visualization of convergence")

# plot the comparaison of the predicted grade with the real one
plt.figure()
plt.plot(real_class, predictions, '.', alpha=0.4)
plt.plot([i for i in range(nGrades)], [i for i in range(nGrades)], 'r--')
plt.xlabel("real grade")
plt.ylabel("predicted grade")
plt.title("Comparaison of the predicted grade and the real one")


# torch.save(model.state_dict(), 'model_vfinal_variation_2_quick_stop_1')
