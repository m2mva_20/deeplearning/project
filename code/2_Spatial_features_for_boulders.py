# -*- coding: utf-8 -*-
"""
Created on Mon Dec 28 10:40:15 2020

@author: Thomas Chapalain
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import json

plt.close('all')

######### Loading the MoonBoard 2017 Json file  & formating the data ##########

with open('Moon17.json') as MoonBoulders:
  jsonDict = json.load(MoonBoulders)
  
Data = jsonDict['Data']
Boulders = list()
for data in Data:
    boulder = dict()
    boulder['Grade'], listMoves, boulder['Angle'] = data['Grade'], data['Moves'], data['Relief']
    holds = list()
    for move in listMoves:
        Xcoordinate, Ycoordinate, isStart, isEnd = move['X'], move['Y'], move['IsStart'], move['IsEnd']
        if isStart:
            holds.append((Xcoordinate, Ycoordinate, 'S'))
        elif isEnd:
            holds.append((Xcoordinate, Ycoordinate, 'E'))
        else :
            holds.append((Xcoordinate, Ycoordinate, 'M'))
    boulder['Holds'] = holds
    Boulders.append(boulder)
    
''' Boulder : { 'Grade' : (str), 'Angle' = (str), 'holds' :  coordinates (x,y) of the holds 
                composing the boulder and a specifier of Start/End/Middle : S/E/M (list) }'''

################ A few statistics about the boulders Dataset ##################
nBoulder = len(Boulders)

###### Distance between each hold of a boulder

def showBoulder(boulder):
    rpz = np.zeros((11, 18))
    holds = boulder['Holds']
    for hold in holds:
        x,y = hold[0]-1, hold[1]-1
        if hold[2] == 'S':
            rpz[x,y] = 1
        elif hold[2] == 'E':
            rpz[x,y] = 2
        else:
            rpz[x,y] = 3
    plt.figure()
    plt.subplot(1,2,1) # Visualisation of the boulder
    plt.title('Boulder difficulty : ' + boulder['Grade'] + ' - angle : ' + boulder['Angle'] +'°', pad=8.)
    plt.imshow(rpz.T, origin='lower')
    plt.xticks([i for i in range(11)],['A','B','C','D','E','F','G','H','I','J','K'])
    plt.yticks([i for i in range(0,18)], [i for i in range(1,19)])
    # Visualisation of the legend
    start_patch = mpatches.Patch(color='steelblue', label='Start')
    top_patch = mpatches.Patch(color='mediumseagreen', label='Top')
    inter_patch = mpatches.Patch(color='gold', label='Authorized')
    plt.legend(title='Holds legend :', bbox_to_anchor=(1.1, 0.4), loc='lower left', borderaxespad=0., handles=[top_patch,inter_patch,start_patch])
    plt.show()
    return None

def isFullyAligned(holds):
    x = holds[0][0]
    X_holds = np.array([hold[0] for hold in holds])
    if np.any(X_holds != x):
        return False
    else:
        return True 
    
def isOneSided(moves):
    side = moves[0][2]
    sides = np.array([move[2] for move in moves])
    if np.any(sides != side):
        return False
    else:
        return True 

def listOfMoves(boulder):
    holds = boulder['Holds']; nHold = len(holds)
    holds.sort(key=lambda x:x[1]) # sort according to Y coordinate of each hold
    top_Hold = holds[nHold-1]
    if not isFullyAligned(holds):
        ''' list of all the consecutive moves in the boulder, except matching on the Top. 
            Takes into account the crossing of hands - Not Perfect but Decent :)
            -> Problems when one hold is  used multiples times or when more than two 
            consecutive holds are X-aligned '''
        listMoves = list()
        i = nHold-1 # the 1st one is always the Top hold
        while i-1 >= 0: 
            current_hold = holds[i]
            previous_hold = holds[i-1]
            j = 1
            while previous_hold[0] == current_hold[0]: # find the first hold non X-aligned with the current one
                previous_hold = holds[i-1-j]
                j += 1
            if j == 1 and previous_hold[1] == current_hold[1]: # find if the previous hold is not Y-aligned with the current one
                ''' In the case of two Top holds : Both are registered in the listOfMoves but only one is returned
                    as the Top hold, therefore, it will by counted twice in the caculus of the accumulative distance '''
                if len(listMoves) != 0: # Case when we have two Top holds
                    del listMoves[-1]
                if current_hold[0] < previous_hold[0]:
                    listMoves.append((previous_hold[0], previous_hold[1], 'R'))
                    listMoves.append((current_hold[0], current_hold[1], 'L'))
                else:
                    listMoves.append((current_hold[0], current_hold[1], 'R'))
                    listMoves.append((previous_hold[0], previous_hold[1], 'L'))
            else:
                if previous_hold[0] < current_hold[0]: # previous_hold on the left
                    last_move = (previous_hold[0], previous_hold[1], 'L')
                    for k in reversed(range(j-1)): # go backward to the current_hold
                        previous_hold = holds[i-1-k]
                        listMoves.append((previous_hold[0], previous_hold[1], 'R'))
                    listMoves.append(last_move)
                else: # previous_hold on the right
                    last_move = (previous_hold[0], previous_hold[1], 'R')
                    for k in reversed(range(j-1)): # go backward to the current_hold
                        previous_hold = holds[i-1-k]
                        listMoves.append((previous_hold[0], previous_hold[1], 'L'))
                    listMoves.append(last_move)
            i -= j
        if not isOneSided(listMoves):
            return listMoves, top_Hold
        else:
            ''' Cas pathologique où toutes les moves se sont vus assignés que Right ou Left '''
            side = listMoves[-1][2]; listMovesFixed = listMoves.copy()
            if side == 'R':
                j = 0
                for move in listMoves:
                    if j%2 == 0: listMovesFixed[j] = (move[0], move[1], 'R')
                    else: listMovesFixed[j] = (move[0], move[1], 'L')
                    j += 1
            else:
                 j = 0
                 for move in listMoves:
                    if j%2 == 0: listMovesFixed[j] = (move[0], move[1], 'L')
                    else: listMovesFixed[j] = (move[0], move[1], 'R')
                    j += 1
            return listMovesFixed, top_Hold
    else:
        ''' Cas pathologique où toutes les prises sont alignées '''
        listMoves = list(); i = 0
        for hold in holds:
            if i%2 == 0:
                listMoves.append((hold[0], hold[1], 'L'))
            else:
                listMoves.append((hold[0], hold[1], 'R'))
            i += 1
        del listMoves[0]
        return listMoves, top_Hold
    
listGrades = ['5+', '6A', '6A+', '6B', '6B+', '6C', '6C+', '7A', '7A+', '7B', '7B+', '7C', '7C+', '8A', '8A+', '8B', '8B+']
nGrades = len(listGrades)

def distanceScore(boulder):
    listMoves, top_Hold = listOfMoves(boulder)
    moves_right_hand = list()
    moves_left_hand = list()
    for move in listMoves:
        if move[2] == 'R':
            moves_right_hand.append(move)
        else:
            moves_left_hand.append(move)
    nR = len(moves_right_hand); nL = len(moves_left_hand)
    distance = 0
    ''' np.exp is used since the difficulty does not increase linearly with the distance
        otherwise, a big move would be as difficult as the sum of a lot a very small moves '''
    for i in reversed(range(1,nR)):
        distance += np.exp(np.sqrt((moves_right_hand[i][0]-moves_right_hand[i-1][0])**2 + (moves_right_hand[i][1]-moves_right_hand[i-1][1])**2))
    for i in reversed(range(1,nL)):
        distance += np.exp(np.sqrt((moves_left_hand[i][0]-moves_left_hand[i-1][0])**2 + (moves_left_hand[i][1]-moves_left_hand[i-1][1])**2))
    ## matching the top hold
    distance += np.exp(np.sqrt((moves_right_hand[0][0]-top_Hold[0])**2 + (moves_right_hand[0][1]-top_Hold[1])**2))
    distance += np.exp(np.sqrt((moves_left_hand[0][0]-top_Hold[0])**2 + (moves_left_hand[0][1]-top_Hold[1])**2))
    return distance


# Distance = list()
# DistanceGrade = list()
# j = 0
# for boulder in Boulders:
#     Distance.append(distanceScore(boulder))
#     for i in range(nGrades):
#         if boulder['Grade'] <= listGrades[i]:
#             break
#     DistanceGrade.append(i)
#     j+=1
    
# plt.figure()
# plt.title('Is the Distance Score representatives of the difficulty ?', pad=16.)
# plt.plot(DistanceGrade, Distance, '.')
# plt.ylabel('distance score of the boulder')
# plt.xlabel('grade')
# plt.xticks([i for i in range(nGrades)], listGrades)
# plt.show()

##### Assymetry score of the boulder

# def assymetry_SE(boulder):
#     listMoves, top_Hold = listOfMoves(boulder)
#     return max(np.abs(top_Hold[0]-listMoves[-1][0]), np.abs(top_Hold[0]-listMoves[-2][0]))

# Assymetry_SE = list()
# AssymetryGrade = list()
# j = 0
# for boulder in Boulders:
#     Assymetry_SE.append(assymetry_SE(boulder))
#     for i in range(nGrades):
#         if boulder['Grade'] <= listGrades[i]:
#             break
#     AssymetryGrade.append(i)
#     j+=1
    
# plt.figure()
# plt.title('Is the assymetry S-E representatives of the difficulty ?')
# plt.plot(AssymetryGrade, Assymetry_SE, '.')
# plt.ylabel('assymetry Start-to-End of the boulder')
# plt.xlabel('grade')
# plt.xticks([i for i in range(nGrades)], listGrades)
# plt.show()

# def assymetry_Tot(boulder):
#     listMoves, top_Hold = listOfMoves(boulder)
#     moves_right_hand = list()
#     moves_left_hand = list()
#     for move in listMoves:
#         if move[2] == 'R':
#             moves_right_hand.append(move)
#         else:
#             moves_left_hand.append(move)
#     nR = len(moves_right_hand); nL = len(moves_left_hand)
#     assymetry = 0
#     for i in reversed(range(1,nR)):
#         assymetry += np.abs(moves_right_hand[i][0]-moves_right_hand[i-1][0])
#     for i in reversed(range(1,nL)):
#         assymetry += np.abs(moves_left_hand[i][0]-moves_left_hand[i-1][0])
#     return assymetry

# Assymetry_Tot = list()
# AssymetryGrade = list()
# j = 0
# for boulder in Boulders:
#     Assymetry_Tot.append(assymetry_Tot(boulder))
#     for i in range(nGrades):
#         if boulder['Grade'] <= listGrades[i]:
#             break
#     AssymetryGrade.append(i)
#     j+=1
    
# plt.figure()
# plt.title('Is the cumulative assymetry representatives of the difficulty ?')
# plt.plot(AssymetryGrade, Assymetry_Tot, '.')
# plt.ylabel('cumulative assymetry of the boulder')
# plt.xlabel('grade')
# plt.xticks([i for i in range(nGrades)], listGrades)
# plt.show()

##### Assymetry of the closest foot to target hold - more reliable assymetry score ?

def assymetry_f(boulder):
    ''' checks the closest reacheable feat under the target hold and scores the 
        resulting assymetry of the body position - exp is used to take in account
        the non linear increase of the difficulty of the move with this score '''
    holds = boulder['Holds']; nHold = len(holds)
    holds.sort(key=lambda x:x[1]) # sort according to Y coordinate of each hold
    Assymetry_f = list()
    for i in range(1,nHold): # takes into account the Top hold 
        target_hold = holds[i]
        x_target = target_hold[0]; y_target = target_hold[1]
        # find the set P of possible foot holds
        P = list()
        for j in reversed(range(i)):
            foot_hold = holds[i-1-j]
            if np.sqrt((foot_hold[0]-x_target)**2 + (foot_hold[1]-y_target)**2) < 10.:
                P.append(foot_hold)
        if not P: 
            assymetry_f = np.exp(11) # P is empty : no foot move - 11 is the span of the MoonBoard
        else :
            assymetry_f = min([np.exp(np.abs(x_target-foot[0])) for foot in P])
        Assymetry_f.append(assymetry_f)
    return Assymetry_f

Assymetry_f_Tot = list()
Assymetry_f_Grade = list()
j = 0
for boulder in Boulders:
    Assymetry_f_Tot.append(sum(assymetry_f(boulder)))
    for i in range(nGrades):
        if boulder['Grade'] <= listGrades[i]:
            break
    Assymetry_f_Grade.append(i)
    j+=1
    
plt.figure()
plt.title('Is the cumulative assymetry_f representatives of the difficulty ?')
plt.plot(Assymetry_f_Grade, Assymetry_f_Tot, '.')
plt.ylabel('cumulative assymetry_f of the boulder')
plt.xlabel('grade')
plt.xticks([i for i in range(nGrades)], listGrades)
plt.show()

##### Debug & test zone
    
# boulder = Boulders[1055]
# listMoves, top_Hold = listOfMoves(boulder)
# print(isOneSided(listMoves))
# holds = boulder['Holds']
# print(isFullyAligned(holds))
# showBoulder(boulder)
# nMoves = len(listMoves) # '+2' for matching on the top_hold & '-2' for the starting position
# assymetry_SE = max(np.abs(top_Hold[0]-listMoves[-1][0]), np.abs(top_Hold[0]-listMoves[-2][0]))

# for boulder in Boulders:
#     if boulder['Grade'] >= '8B':
#         showBoulder(boulder)

