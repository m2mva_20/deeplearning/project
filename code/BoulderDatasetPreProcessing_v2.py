# -*- coding: utf-8 -*-
"""
Created on Sat Jan 23 10:21:26 2021

@author: Thomas Chapalain
"""

import numpy as np
from numpy import random as rd
import matplotlib.pyplot as plt
import json

plt.figure()
plt.close('all')

import torch

######### Loading the MoonBoard 2017 Json file  & formating the data ##########

with open('Moon17.json') as MoonBoulders:
  jsonDict = json.load(MoonBoulders)
  
Data = jsonDict['Data']
Boulders = list()
for data in Data:
    boulder = dict()
    boulder['Grade'], listMoves, boulder['Angle'] = data['Grade'], data['Moves'], data['Relief']
    holds = list()
    for move in listMoves:
        Xcoordinate, Ycoordinate, isStart, isEnd = move['X'], move['Y'], move['IsStart'], move['IsEnd']
        if isStart:
            holds.append((Xcoordinate, Ycoordinate, 'S'))
        elif isEnd:
            holds.append((Xcoordinate, Ycoordinate, 'E'))
        else :
            holds.append((Xcoordinate, Ycoordinate, 'M'))
    boulder['Holds'] = holds
    Boulders.append(boulder)
    
''' Boulder : { 'Grade' : (str), 'Angle' = (str), 'holds' :  coordinates (x,y) of the holds 
                composing the boulder and a specifier of Start/End/Middle : S/E/M (list) }'''
    
class DatasetPreProcessing():
    
    def __init__(self, BouldersDataset):
        self.nBoulder = len(BouldersDataset)
        self.Grades = self.GradesRaw(BouldersDataset)
        self.nGrades = len(self.Grades) 
        self.listGrades = list(self.Grades.keys()); self.listGrades.sort() # alphanumeric order
        self.holdsMatrix = self.holdsDifficultyScore(BouldersDataset)
        self.preProcessedDataset25, self.preProcessedDataset40  = self.preProcessDataset(BouldersDataset)
        self.weigthsForImbalancedClass25 = self.showGradesDistribution(self.preProcessedDataset25)
        self.weigthsForImbalancedClass40 = self.showGradesDistribution(self.preProcessedDataset40)
        
    ##### Methods useful for the pre-processing of the Grade of each Boulder 
    def GradesRaw(self, BouldersDataset):
        GradesRawDict = dict()
        for boulder in Boulders:
            grade = boulder['Grade']
            if grade not in GradesRawDict.keys():
                GradesRawDict[grade] = 1
            else:
                GradesRawDict[grade] += 1
        return GradesRawDict
    
    def holdsDifficultyScore(self, BouldersDataset):
        ''' Score of each hold in the 18x11 matrix composing the MoonBoard 2017 '''
        nX, nY = 11, 18
        self.dictGrades = {}
        for grade in self.listGrades:
            self.dictGrades[grade] = 0
            
        holdsMatrix = [[self.dictGrades.copy() for i in range(nY)] for i in range(nX)]
        for boulder in BouldersDataset:
            holds = boulder['Holds']
            for hold in holds:
                x, y = hold[0]-1, hold[1]-1
                holdsMatrix[x][y][boulder['Grade']] += 1
        return holdsMatrix

    def weightedMeanGrade(self, hold):
        i = 1; score = 0; W = 0
        for grade in list(hold.keys()):
            nG = self.Grades[grade]; ng = hold[grade]; weight = ng/nG; W += weight
            score += i*weight
            i += 1
        return list(hold.keys())[int(score/W)-1]    

    def grades(self, boulder):
        holds = boulder['Holds']
        holds.sort(key=lambda x:x[1]) # sort according to Y coordinate of each hold
        grades_list = list()
        for hold in holds:
            x, y = hold[0]-1, hold[1]-1
            alphaNumericGrade = self.weightedMeanGrade(self.holdsMatrix[x][y])
            for i in range(self.nGrades):
                if alphaNumericGrade == self.listGrades[i]:
                    break
            grades_list.append(i)
        return grades_list
    
    ##### Methods useful for the pre-processing of the Distance of the moves of each Boulder 
    def distanceMoves(self, boulder):
        holds = boulder['Holds']; nHold = len(holds)
        holds.sort(key=lambda x:x[1]) # sort according to Y coordinate of each hold
        DistanceMoves = list()
        for i in range(1,nHold): # takes into account the Top hold 
            target_hold = holds[i]
            current_hold = holds[i-1]
            distanceMove = np.sqrt((target_hold[0]-current_hold[0])**2 + (target_hold[1]-current_hold[1])**2)
            DistanceMoves.append(distanceMove)
        DistanceMoves.append(distanceMove) # don't forget to match the Top hold !
        return DistanceMoves

    ##### Methods useful for the pre-processing of the Distance of the moves of each Boulder 
    def assymetry_f(self, boulder):
        ''' checks the closest reacheable feat under the target hold and scores the 
            resulting assymetry of the body position'''
        holds = boulder['Holds']; nHold = len(holds)
        holds.sort(key=lambda x:x[1]) # sort according to Y coordinate of each hold
        Assymetry_f = list()
        for i in range(1,nHold): # takes into account the Top hold 
            target_hold = holds[i]
            x_target = target_hold[0]; y_target = target_hold[1]
            # find the set P of possible foot holds
            P = list()
            for j in reversed(range(i)):
                foot_hold = holds[i-1-j]
                if np.sqrt((foot_hold[0]-x_target)**2 + (foot_hold[1]-y_target)**2) < 10.:
                    P.append(foot_hold)
            if not P: 
                assymetry_f = 11 # P is empty : no foot move - 11 is the span of the MoonBoard
            else :
                assymetry_f = min([np.abs(x_target-foot[0]) for foot in P])
            Assymetry_f.append(assymetry_f)
        # don't forget to match the Top hold !
        Assymetry_f.append(assymetry_f) 
        return Assymetry_f

    ###### Chosen features about the boulders Dataset            
    def features(self, boulder):
        grades_list = self.grades(boulder)
        distanceMove_list = self.distanceMoves(boulder)
        assymetry_list = self.assymetry_f(boulder)
        features_list = list()
    
        nFeature = len(grades_list)
        for i in range(nFeature):
            features_list.append((grades_list[i], distanceMove_list[i], assymetry_list[i]))            
        return features_list        
    
    def preProcessDataset(self, BouldersDataset):
        ''' return the list of features for each boulder in the Dataset as a tensor 
            as well as the corresponding tensor of the associated grades '''
        Dataset25, Dataset40 = list(), list()
        for boulder in BouldersDataset:
            for i in range(self.nGrades):
                if boulder['Grade'] <= self.listGrades[i]:
                    break
            steepness = int(boulder['Angle'])
            if steepness == 40:
                Dataset40.append((torch.Tensor(self.features(boulder)).float(), torch.tensor([i]).long(), steepness))
            else:
                Dataset25.append((torch.Tensor(self.features(boulder)).float(), torch.tensor([i]).long(), steepness))
        return Dataset25, Dataset40
    
    ##### Balance the preProcessed dataset so that the imbalance issue of the grades is addressed
    def balancedProcessedSubDataset(self, processedDataset, nBoulder):
        pBoulders = processedDataset.copy()
        rd.shuffle(pBoulders)
        
        balancedDataset = list()
        countGradesLeft = [int(nBoulder/self.nGrades)]*self.nGrades
        
        for boulder in pBoulders:
            grade = boulder[1].item()
            if countGradesLeft[grade] > 0:
                countGradesLeft[grade] -= 1
                balancedDataset.append(boulder)
        
        return balancedDataset
        
            
    ##### Visualisation of the dataset's grades distribution 
    def showGradesDistribution(self, dataset):
        listGrades = list()
        for boulder in dataset:
            listGrades.append(boulder[1].item())
        countGrades = np.ones(self.nGrades)
        for grade in listGrades:
            countGrades[grade] += 1
        plt.figure()
        plt.title('Distribution of the grades in the Dataset{} - nBoulder = {}'.format(boulder[2], len(dataset)))
        plt.bar([i for i in range(self.nGrades)], countGrades-1, tick_label=self.listGrades)
        plt.show()
        return torch.Tensor(len(dataset)/countGrades).float()
    
    def comparaisonPredictedGradesDistribution(self, dataset, predictedGrades):
        listGrades = list()
        for boulder in dataset:
            listGrades.append(boulder[1].item())
        countGrades = np.ones(self.nGrades)
        for grade in listGrades:
            countGrades[grade] += 1
            
        countPredictedGrades = np.ones(self.nGrades)
        for grade in predictedGrades:
            countPredictedGrades[grade] += 1
            
        x = np.array([i for i in range(self.nGrades)])
        width = 0.35  # the width of the bars
        
        fig, ax = plt.subplots()
        ax.bar(x - width/2, countGrades-1, width, label='Dataset Distribution')
        ax.bar(x + width/2, countPredictedGrades, width, label='Predicted Grades')
        ax.set_title('Distribution of the grades in the Dataset{} - nBoulder = {}'.format(boulder[2], len(dataset)))
        ax.set_xticks(x)
        ax.set_xticklabels(self.listGrades)
        ax.legend()
        
        return np.array(countGrades)/np.array(countPredictedGrades)
    