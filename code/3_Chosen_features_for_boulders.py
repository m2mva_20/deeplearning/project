# -*- coding: utf-8 -*-
"""
Created on Sun Jan  3 18:54:29 2021

@author: Thomas Chapalain
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import json

plt.close('all')

######### Loading the MoonBoard 2017 Json file  & formating the data ##########

with open('Moon17.json') as MoonBoulders:
  jsonDict = json.load(MoonBoulders)
  
Data = jsonDict['Data']
Boulders = list()
for data in Data:
    boulder = dict()
    boulder['Grade'], listMoves, boulder['Angle'] = data['Grade'], data['Moves'], data['Relief']
    holds = list()
    for move in listMoves:
        Xcoordinate, Ycoordinate, isStart, isEnd = move['X'], move['Y'], move['IsStart'], move['IsEnd']
        if isStart:
            holds.append((Xcoordinate, Ycoordinate, 'S'))
        elif isEnd:
            holds.append((Xcoordinate, Ycoordinate, 'E'))
        else :
            holds.append((Xcoordinate, Ycoordinate, 'M'))
    boulder['Holds'] = holds
    Boulders.append(boulder)
    
''' Boulder : { 'Grade' : (str), 'Angle' = (str), 'holds' :  coordinates (x,y) of the holds 
                composing the boulder and a specifier of Start/End/Middle : S/E/M (list) }'''

def showBoulder(boulder):
    rpz = np.zeros((11, 18))
    holds = boulder['Holds']
    for hold in holds:
        x,y = hold[0]-1, hold[1]-1
        if hold[2] == 'S':
            rpz[x,y] = 1
        elif hold[2] == 'E':
            rpz[x,y] = 2
        else:
            rpz[x,y] = 3
    plt.figure()
    plt.subplot(1,2,1) # Visualisation of the boulder
    plt.title('Boulder difficulty : ' + boulder['Grade'] + ' - angle : ' + boulder['Angle'] +'°', pad=8.)
    plt.imshow(rpz.T, origin='lower')
    plt.xticks([i for i in range(11)],['A','B','C','D','E','F','G','H','I','J','K'])
    plt.yticks([i for i in range(0,18)], [i for i in range(1,19)])
    # Visualisation of the legend
    start_patch = mpatches.Patch(color='steelblue', label='Start')
    top_patch = mpatches.Patch(color='mediumseagreen', label='Top')
    inter_patch = mpatches.Patch(color='gold', label='Authorized')
    plt.legend(title='Holds legend :', bbox_to_anchor=(1.1, 0.4), loc='lower left', borderaxespad=0., handles=[top_patch,inter_patch,start_patch])
    plt.show()
    return None    
    

nBoulder = len(Boulders)
listGrades = ['5+', '6A', '6A+', '6B', '6B+', '6C', '6C+', '7A', '7A+', '7B', '7B+', '7C', '7C+', '8A', '8A+', '8B', '8B+']
nGrades = len(listGrades)


##### Distribution of the grades throughtout the Dataset - Raw
GradesRaw = dict()
for boulder in Boulders:
    grade = boulder['Grade']
    if grade not in GradesRaw.keys():
        GradesRaw[grade] = 1
    else:
        GradesRaw[grade] += 1
        
listGradesRaw = list(GradesRaw.keys()); nGrades = len(listGradesRaw); listGradesRaw.sort() # alphanumeric order
countGradesRaw = list()
for grade in listGradesRaw:
    countGradesRaw.append(GradesRaw[grade])
  
plt.figure()
plt.title('Distribution of the grades in the Dataset - nBoulder = {}'.format(nBoulder))
plt.bar([i for i in range(nGrades)], countGradesRaw, tick_label=listGradesRaw)
plt.show()

##### Score of each hold in the 18x11 matrix composing the MoonBoard 2017
nX, nY = 11, 18
dictGrades = {}
for grade in listGradesRaw:
    dictGrades[grade] = 0
    
holdsMatrix = [[dictGrades.copy() for i in range(nY)] for i in range(nX)]
for boulder in Boulders:
    holds = boulder['Holds']
    for hold in holds:
        x, y = hold[0]-1, hold[1]-1
        holdsMatrix[x][y][boulder['Grade']] += 1
        
def weightedMeanGrade(hold):
    i = 1; score = 0; W = 0
    for grade in list(hold.keys()):
        nG = GradesRaw[grade]; ng = hold[grade]; weight = ng/nG; W += weight
        score += i*weight
        i += 1
    return list(hold.keys())[int(score/W)-1]

weightedMeanGradeDict = dictGrades.copy()
for i in range(nX):
    for j in range(nY):
        weightedMean = weightedMeanGrade(holdsMatrix[i][j])
        weightedMeanGradeDict[weightedMean] += 1

countWeightedMeanGrades = list(weightedMeanGradeDict.values()) # fonctionne vu que le dict.keys() est ordonné alphanumériquement

plt.figure()
plt.title('Distribution of the hold weighted mean grade - Moon 2017')
plt.bar([i for i in range(nGrades)], countWeightedMeanGrades, tick_label=listGradesRaw)
plt.show()

def grades(boulder):
    holds = boulder['Holds']
    holds.sort(key=lambda x:x[1]) # sort according to Y coordinate of each hold
    grades_list = list()
    for hold in holds:
        x, y = hold[0]-1, hold[1]-1
        alphaNumericGrade = weightedMeanGrade(holdsMatrix[x][y])
        for i in range(nGrades):
            if alphaNumericGrade == listGrades[i]:
                break
        grades_list.append(i)
    return grades_list

###### Distance between each hold of a boulder

def distanceMoves(boulder):
    holds = boulder['Holds']; nHold = len(holds)
    holds.sort(key=lambda x:x[1]) # sort according to Y coordinate of each hold
    DistanceMoves = list()
    for i in range(1,nHold): # takes into account the Top hold 
        target_hold = holds[i]
        current_hold = holds[i-1]
        distanceMove = np.sqrt((target_hold[0]-current_hold[0])**2 + (target_hold[1]-current_hold[1])**2)
        DistanceMoves.append(distanceMove)
    DistanceMoves.append(distanceMove) # don't forget to match the Top hold !
    return DistanceMoves

Distance = list()
DistanceGrade = list()
j = 0
for boulder in Boulders:
    nMoves = len(boulder['Holds'])
    Distance.append(sum(distanceMoves(boulder))/nMoves)
    for i in range(nGrades):
        if boulder['Grade'] <= listGrades[i]:
            break
    DistanceGrade.append(i)
    j+=1
    
plt.figure()
plt.title('Is the Distance Score representatives of the difficulty ?', pad=16.)
plt.plot(DistanceGrade, Distance, '.')
plt.ylabel('distance score of the boulder')
plt.xlabel('grade')
plt.xticks([i for i in range(nGrades)], listGrades)
plt.show()

##### Assymetry of the closest foot to target hold

def assymetry_f(boulder):
    ''' checks the closest reacheable feat under the target hold and scores the 
        resulting assymetry of the body position - exp is used to take in account
        the non linear increase of the difficulty of the move with this score '''
    holds = boulder['Holds']; nHold = len(holds)
    holds.sort(key=lambda x:x[1]) # sort according to Y coordinate of each hold
    Assymetry_f = list()
    for i in range(1,nHold): # takes into account the Top hold 
        target_hold = holds[i]
        x_target = target_hold[0]; y_target = target_hold[1]
        # find the set P of possible foot holds
        P = list()
        for j in reversed(range(i)):
            foot_hold = holds[i-1-j]
            if np.sqrt((foot_hold[0]-x_target)**2 + (foot_hold[1]-y_target)**2) < 10.:
                P.append(foot_hold)
        if not P: 
            assymetry_f = 11 # P is empty : no foot move - 11 is the span of the MoonBoard
        else :
            assymetry_f = min([np.abs(x_target-foot[0]) for foot in P])
        Assymetry_f.append(assymetry_f)
    # don't forget to match the Top hold !
    Assymetry_f.append(assymetry_f) 
    return Assymetry_f

Assymetry_f_Tot = list()
Assymetry_f_Grade = list()
j = 0
for boulder in Boulders:
    nMoves = len(boulder['Holds'])
    Assymetry_f_Tot.append(sum(assymetry_f(boulder))/nMoves)
    for i in range(nGrades):
        if boulder['Grade'] <= listGrades[i]:
            break
    Assymetry_f_Grade.append(i)
    j+=1
    
plt.figure()
plt.title('Is the cumulative assymetry_f representatives of the difficulty ?')
plt.plot(Assymetry_f_Grade, Assymetry_f_Tot, '.')
plt.ylabel('cumulative assymetry_f of the boulder')
plt.xlabel('grade')
plt.xticks([i for i in range(nGrades)], listGrades)
plt.show()

###### Chosen features about the boulders Dataset 
        
def features(boulder, showBloc=False):
    steepness = float(boulder['Angle'])
    grades_list = grades(boulder)
    distanceMove_list = distanceMoves(boulder)
    assymetry_list = assymetry_f(boulder)
    features_list = list()

    nFeature = len(grades_list)
    for i in range(nFeature):
        features_list.append((grades_list[i], distanceMove_list[i], assymetry_list[i], steepness))

    if showBloc:
        showBoulder(boulder)
        
    return features_list

##### Debug & test zone

hard_ones = list()
for boulder in Boulders:
    if boulder['Grade'] >= '8B+':
        hard_ones.append(boulder)
        
easy_ones = list()
for boulder in Boulders:
    if boulder['Grade'] <= '5+':
        easy_ones.append(boulder)
