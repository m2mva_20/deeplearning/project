% This version of CVPR template is provided by Ming-Ming Cheng.
% Please leave an issue if you found a bug:
% https://github.com/MCG-NKU/CVPR_Template.

%\documentclass[review]{template/latex/cvpr}
\documentclass[final]{template/latex/cvpr}

\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[pagebackref=true,breaklinks=true,colorlinks,bookmarks=false]{hyperref}


\def\cvprPaperID{1} % *** Enter the CVPR Paper ID here
\def\confYear{MVA 2021}
%\setcounter{page}{4321} % For final version only


\begin{document}

%%%%%%%%% TITLE
\title{Deep learning project proposition :\\ Classification of rock climbing
  routes difficulty}


\author{Nicolas \textsc{Atienza}\\
Master MVA\\
ENS Paris Saclay\\
{\tt\small nicolas.atienza@ens-paris-saclay.fr}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
\and
Thomas \textsc{Chapalain}\\
Master MVA\\
ENS Paris Saclay\\
{\tt\small thomas.chapalain@ens-paris-saclay.fr}
}

\maketitle

\section{Introduction}

This project aims to develop deep learning models in order to learn rock
climbing routes difficulty.

\subsection{Motivation}
In climbing, and more specifically in bouldering which is a part of the sport where routes are usually shorter and ascended without ropes, difficulty grades are very important for skill assessment, enjoyment and safety. Indeed, climbers use these grades to benchmark their performance on climbs. Thus, finding inaccurately graded boulders can
be frustrating and detrimental to their ability to assess their strenghts and weaknesses. More seriously, climbing is considered as an extreme sport where climbers can allegedly face high-risk situations which can result into severe injuries for an unprepared climber who attemtps a route above its range of performance due to a misjudment about its grade.

However, classifying the difficulty of climbing routes is a challenging and subjective task, even for experienced
climbers. In this project, the main goal would be to develop an algorithm which would classify with high accuracy boulders based upon their difficulty in order to standardize these grades.


\subsection{Problem definition}
Due to the recent rise of popularity for this sport, the number of newly opened climbing gyms has soared. They often have a spray wall covered with holds which are left in free access for climber when they want to create their own boulders. In this project, we focus on classifying these self-made indoors boulders which often does not have an accurate grade due to the lack of experience of the usual climber in routesetting.

We will use the Fontainebleau grading system because
it is the most widely used internationally. In this system, bouldering routes are graded on an alphanumeric scale according to the following format \textit{number-letter-sign}. The
number scale ranges from 1 to 9 in an increasing order of
difficulty. Within a number level, the following letter (A, B
or C) ranks the routes in increasing order of difficulty. Finally, a + sign can be added afterwards to give some more
granularity (a 7C+ should be more difficult than a 7C, but
less than a 8A).

\subsection{Related work}


To our knowledge, the existing liteature in machine learning about the
classification of climbing routes is limited. We could only find the following
papers \cite{TaiCS230} and \cite{stanfordClimb}  which attempts to tackle this problem using several different approaches. However, the currently proposed algorithms all lag behind human classification
performance, reaching peak performance with only $35\%$ top accuracy.

In \cite{stanfordClimb}, three different models were explored for classification : Naive Bayes, softmax regression, and
a convolutional neural-network (CNN) with an ordinal output. Meanwhile, in \cite{colas}, two main approaches were considered : the first one uses an end-to-end CNN architecture working directly from route images. The second approach proposes using a CNN encoding stage to build "hold embeddings", a vector representation of holds, from hold images.

However, with the strategies presented in these papers, the structure of the input data was never at the center of the study and no \textit{a priori} information about the possible features of a bouldering route that influence its difficulty was used. The current strategies mainly rely on the complexity and on large amount of parameters composing their neural network to catch these features. We think that it might be a reason that account for the poor performances of the proposed algorithms in term of grading accuracy.



\section{Methodology}

In order to adress this alledge weakness identified in the previous papers, we plan to implement two different and opposite approaches which will put a greater emphasis on the struture of the input data. First, considering a boulder as a graph of connected holds, one strategy will be to implement a GCN classifier whereas a second approach will focus on creating an input space of the \textit{a priori} relevant features of a boulder that influence its difficulty and then use it to train more efficiently a simpler classification network, for exemple, a mixture density network.


\subsection{Dataset}
The MoonBoard is a standardized
bouldering wall which can be found in many climbing gyms
around the world. It consists of a set of up to $n = 198$ holds
placed on a $18\times11$ grid. The idea is that every member of the moonboard
community can create and grade climbing routes. Hence, the moonboard website
regroup thousands of publicly accessible graded routes. Each route is described
by the grade given by setter, the grade given by the community's users and the
succession of hold's coordinate of the route.


\begin{figure}[!h]
\centering
\includegraphics[width=0.4 \linewidth]{fig/moonboard}
\caption{Example of a Moonboard bouldering problem. Green circles are for starting
holds, blue ones are for intermediate holds and red ones are for finish holds.}
\end{figure}

We plan to use data scrapped from moonboard website on the 2017 set-up in order to
train and validate our models. That represent roughly 30000 different graded routes. As there is much more intermediate climbers than
expert one, we plan to sample among this dataset a training sample and a
validation sample in which all grade are uniformly represented.

\subsection{Routes classification by GCN classifier}

Graph neural network, or grah embeddings, as defined in
\cite{battaglia2018relational}, propose a framework that have recently shown
great performance in classification task as it enables to create embeddings with
lot of relationnal information that preserve the global structure of the
information. Such model have been used in \cite{yao2018graph} in order to
perform text classification. The idea was to adapt the graph convolutionnal
network model (GCN) introduced by \cite{kipf2017semisupervised} to text by
representing words and documents in a same graph. This work have been employed
in \cite{TaiCS230} in order to adress the classification problem of the rock
climbing routes. The fundamental idea of this work was to create an analogy
between words and climbing holds. That is this algorithm that we first propose to
implement in this project.

The implementation of this method will consist in two tasks. First we plan to
propose a graph model of a moonboard climbing route different from the one used
in \cite{TaiCS230} but still based on the construction of the text GCN
developped in \cite{yao2018graph}. Then, we will feed this graph model into a
two layer GCN as in \cite{kipf2017semisupervised}. 


\section{Evaluation}

In this project we propose to evaluate the performance of our models by
computing the error between the prediction and the grade given by the setter of
the route. According to litterature \cite{TaiCS230}, \cite{stanfordClimb}, all
existing models are achieving at best $35\%$ of accuracy while the users ratings
are achieving $90\%$. Hence the accuracy of our models will be compared with the
accuracy reached by the community users and with this literature limit of $35\%$. 

{\small
\bibliographystyle{ieee_fullname}
\bibliography{biblio}
}

\end{document}