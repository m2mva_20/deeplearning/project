% This version of CVPR template is provided by Ming-Ming Cheng.
% Please leave an issue if you found a bug:
% https://github.com/MCG-NKU/CVPR_Template.

%\documentclass[review]{Template/cvpr}
\documentclass[final]{cvpr}

\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{caption}
\usepackage{subcaption}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[pagebackref=true,breaklinks=true,colorlinks,bookmarks=false]{hyperref}


\def\cvprPaperID{1} % *** Enter the CVPR Paper ID here
\def\confYear{MVA 2021}
%\setcounter{page}{4321} % For final version only

\begin{document}

%%%%%%%%% TITLE
\title{Classification of bouldering routes difficulty}


\author{Nicolas \textsc{Atienza}\\
Master MVA\\
ENS Paris Saclay\\
{\tt\small nicolas.atienza@ens-paris-saclay.fr}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
\and
Thomas \textsc{Chapalain}\\
Master MVA\\
ENS Paris Saclay\\
{\tt\small thomas.chapalain@ens-paris-saclay.fr}
}

\maketitle

\begin{abstract}
  This work has been carried out as a final project of the Deep Learning course of
  the MVA Master at ENS Paris Saclay. In this work, we propose an original approach
  to the classification problem of rock climbing routes. Our approach enables to
  almost reach state of the art accuracy using a simple shallow neural
  network that can easily be trained on any computer CPU. To tackle this
  classification problem, we first proposed to reduce the dimension of the input space by representing each boulder as a sequence of a small amount of features that were considered discriminative for assessing its difficulty.  Then, we designed two types of
   models, based on simple dense neural
  networks, that take as input these chosen features and output the predicted
  difficulty of the boulder as a grade. The training and the evaluation of our models have been performed on the
  moonboard 2017 dataset. All the code related to this project can be found on
  this gitlab repository: \url{https://gitlab.com/m2mva_20/deeplearning/project}. 
\end{abstract}

\section{Introduction}

In climbing, and more specifically in bouldering which is a part of the sport where routes are usually shorter and ascended without ropes, difficulty grades are very important for skill assessment, enjoyment and safety. Indeed, climbers use these grades to benchmark their performance on climbs. Thus, finding inaccurately graded boulders can
be frustrating and detrimental to their ability to assess their strenghts and weaknesses. More seriously, climbing is considered as an extreme sport where climbers can allegedly face high-risk situations which can result into severe injuries for an unprepared climber who attemtps a route above its range of performance due to a misjudment about its grade.\\

Moreover, classifying the difficulty of climbing routes is a challenging and subjective task, even for experienced
climbers. In this project, the main goal would be to develop an algorithm which would classify with high accuracy boulders based upon their difficulty in order to standardize these grades.


\section{Problem definition}

Due to the recent rise of popularity for this sport, the number of newly opened climbing gyms has soared. They often have a spray wall covered with holds which are left in free access for climbers when they want to create their own boulders. In this project, we focus on classifying these self-made indoors boulders which often does not have an accurate grade due to the lack of experience of the usual climber in routesetting.\\

We will use the Fontainebleau grading system because
it is the most widely used internationally. In this system, bouldering routes are graded on an alphanumeric scale according to the following format \textit{number-letter-sign}. The
number scale ranges from 1 to 9 in an increasing order of
difficulty. Within a number level, the following letter (A, B
or C) ranks the routes in increasing order of difficulty. Finally, a + sign can be added afterwards to give some more
granularity (a 7C+ should be more difficult than a 7C, but
less than a 8A).

\section{Related work}

To our knowledge, the existing literature in machine learning about the
classification of climbing routes is limited. We could only find the following
papers \cite{TaiCS230} and \cite{stanfordClimb}  which attempts to tackle this problem using several different approaches. However, the currently proposed algorithms all lag behind human classification
performance, reaching peak performance with only $35\%$ to $40\%$ top accuracy.\\

In \cite{stanfordClimb}, three different models were explored for classification : Naive Bayes, softmax regression, and
a convolutional neural-network (CNN) with an ordinal output. Meanwhile, in \cite{colas}, two main approaches were considered : the first one uses an end-to-end CNN architecture working directly from route images. The second approach proposes to use a CNN encoding stage to build "hold embeddings", a vector representation of holds, from hold images.\\

However, with the strategies presented in these papers, the structure of the input data was never at the center of the study and no \textit{a priori} information about the possible features of a bouldering route that influence its difficulty was used. The current strategies mainly rely on the intricacy and on the large amount of parameters composing their neural network to catch these features. We think that it might be a reason that account for the poor performances of the proposed algorithms in term of grading accuracy.\\

Indeed, the best performing methods in the literature feed the image of the Moonboard, as shown in \figurename~\ref{moonboard}, annotated with the holds used in the boulder and a label representing its grade to a deep CNN network with approximately 50 000 independent parameters. However, as one can see from \figurename~\ref{comparaisonBoulder}, it is very difficult to differentiate the grade of two boulders relying on the spatial information contained in the disposition of their associated holds - even if they are very far apart on the grading scale. Therefore, the convolutional layers of the CNN will not benefit from an image-like representation of the boulder since the relevant spatial information is difficult to infer with this approach. Indeed, spatial information is only expressed locally as the position of the holds on the Moonboard but these positions present no correlation amongst themselves since different subsets of holds having a spatial disposition close to each other can be associated with boulders of very different grade depending on the inherent difficulty of each hold composing the subset.

\begin{figure}[!]
\centering
\includegraphics[width=0.4 \linewidth]{fig/moonboard}
\caption{Example of a Moonboard bouldering problem. Green circles are for starting
  holds, blue ones are for intermediate holds and red ones are for finish holds.}
\label{moonboard}
\end{figure}

\begin{figure}[!]
\centering
\includegraphics[width= \linewidth]{fig/comparaison_boulder}
\caption{Comparaison of two extremely different boulders (most easiest / most difficult one) from spatial information.}
\label{comparaisonBoulder}
\end{figure}



\section{Methodology}

In order to address this alledge weakness identified in the previous papers, we
plan to design and implement a different approach that will put a greater
emphasis on the strucure of the input data. To do so, we first propose to
define some features that might be able to represent the difficulty of a
climbing routes. Then, we propose two different neural network architectures that can be trained to
predict the grade of a route using these features as an input. 

\subsection{Selected features definition}
As discussed in the previous sections, our approach puts the emphasis on selecting a small amount of relevant features that will be discriminative regarding the difficulty of each boulder. This strategy has the underlying objective of reducing drastically the dimension of the inputs that will be fed to the neural network without being detrimental to the accuracy of the prediction.\\

From our own experience of bouldering, the difficulty of a boulder comes from the difficulty of the moves that constitute it ie. the sequence of movements between the pairs of holds composing the boulder. Moreover, each individual move becomes increasingly harder when the holds involved are more difficult to grab, are farther apart or force a resulting body position that is very unnatural, which we will call the assymetry of the movement, for exemple, a move that forces the climber to put a \textit{high heel hook}, thus, resulting in a body position where one of the foot is at the same height of the hands is likely to be an advanced move that a beginner would not be able to do.\\

If the distance between the holds composing a move is easily inferred from their X/Y coordinates on the Moonboard (see \figurename~\ref{moonboard}), the two remaining features needs to be approximated as there does not exist any closed-form formula to compute them.\\

In order to infer a score representing the difficulty of a hold, one can remark that if a particular hold is only used in the very difficult boulders, it is likely to be inherently difficult - very small or very hard to grab such as a \textit{bad sloper} or a \textit{mono-pocket}. Therefore, we will give to each hold a fictive grade computed as the average \textbf{weighted} grade over the dataset of all the boulders which use this particular hold. The weights used in this mean account for the imbalance of the class distribution in the dataset ie. a greater weight will be given to a difficult boulder compared to an easier one since they are less represented. (cf section \textbf{4.2})\\

For the assymetry, a possible score that can approximate this feature can be the ease of the climber to find a foot hold once the target hold is reached. Indeed, one can remark that it is often the lack of foot holds that force the climber to use complex techniques such as \textit{heel hook} or even to \textit{campus} ie. reaching the next hold only using the strength of his arms. Therefore, we compute this assymetry score as the horizontal distance to the closest foot hold from the target hold. If no such foot hold exist, a max assymetry score of 11 is assigned to the move, representing a mandatory \textit{campus} move.\\

Since the Moonboard can be inclined by 25 or 40 degrees, another important feature that will be used as a \textit{switching context} parameter is the steepness of the climbing wall. Indeed, it is clear that the same sequence of moves will be much harder on a $40^{\circ}-$ wall than on a $25^{\circ}-$ wall.\\


\begin{figure}[!]
\centering
\includegraphics[width=\linewidth]{fig/features}
\caption{Description of the chosen features used to represent a boulder. The distance (resp. assymetry) of a move is shown as a red (resp. blue) arrow and the inherent difficulty of a hold is described by the heatMap of mean difficulty inferred across the whole dataset.}
\label{features}
\end{figure}

In conclusion, a boulder will be represented as sequence of moves, each move being composed of the three following features : distance, grade, assymetry. A schematic description of these features is represented \figurename~\ref{features}. This choice of features assumes that the difficulty of a move and of a boulder can be expressed as follow :

\[ \left\lbrace \begin{array}{lcl}
\text{d}_{move}^{(i)} & = & f_{s^{(i)}}\left(~\!\textit{dist}^{(i)},~\!\textit{grade}^{(i)},~\! \textit{assym}^{(i)}~\!\right) \\ \\
\text{d}_{boulder} & = & g\left(~\!d_{move}^{(1)}~\!,~\!\cdots,~\!d_{move}^{(N)}~\!~\!\right)
\end{array}\right. \]
where $s^{(i)}$ accounts for the steepness parameter used as a \textit{switching context} parameter. (see section \textbf{4.4} for details)
\subsection{Dataset}
The MoonBoard is a standardized
bouldering wall which can be found in many climbing gyms
around the world. It consists of a set of up to $n = 198$ holds
placed on a $18\times11$ grid. The idea is that every member of the moonboard
community can create and grade climbing routes. Hence, the moonboard website
regroup thousands of publicly accessible graded routes.\\

We are going to train and evaluate the performances of our models on data
scrapped from moonboard website on the 2017 set-up in order to be able to
compare our performance to the literature. This dataset is composed of 30045
different graded routes. This dataset has several inherent difficulties that one has to work with when training a neural network on it. First, the grades distribution throughtout this dataset is very unbalanced as one can see in \figurename~\ref{distribution}. This is
the direct consequence of the fact that most climbers are neither experts nor beginners and
climb routes of medium difficulty around \texttt{6A/B}. \\

This specificity of the dataset is
making the learning process quite hard, especially for the most difficult routes that
are not well-represented in the dataset. Indeed, one can not easily apply classical techniques of data augmentation to address this issue since the Moonboard itself does not present any symmetry. Although upsampling the less common classes seems to be a pratical solution, it is risky as the neural network is likely to overfit these classes and not be able to generalize when asked to predict the grade of a never seen difficult boulder whose input features would be sightly different from the upsampled ones.\\

Another difficulty of the dataset arises from the representation of each boulder as a sequence of features whose lenght is specific to the latter. It implies that during the training phase of the neural network model, PyTorch Dataloaders can not be used to perform efficient mini-batch training as these built-in functions do not support variable-sized inputs. This specificity of the dataset compelled us to implement our very own Dataloarder and to pay utmost attention to the architecture of the proposed neural network models to ensure compatibility with our custom Dataloader. 

\begin{figure}[!h]
  \centering
  \includegraphics[width=.85\linewidth]{fig/distribution.png}
  \caption{Distribution of the grades in the moonboard 2017 dataset.}
  \label{distribution}
\end{figure}


\subsection{Deep forward regression model}
This proposed architecture only serves as a benchmark for the performances one can obtained without trying to design a specific neural network for the bouldering routes classification task. Therefore, the architecture remains very simple on purpose. This model is a classic deep feed forward neural network created by stacking many fully connected linear layers of various width with ReLu or PReLu activation functions. 


\subsection{Shallow softmax classifier model}
The design of the architecture of this neural network was driven by the priors we had about the target function it has to approximate. In this section, we will present these priors as well as the three main stages composing this model. To help in the understanding of this architecture, a schematic reprensentation of the network is given in \figurename~\ref{arch_soft_max}.\\

\underline{$1^{st}$ stage} : The first stage of the network, called \textit{NL function approx}, arises from the following remark. When climbing a boulder, it is clear that the difficulty of a move does not increase linearly with the distance between the holds involved. Otherwise, the difficulty of a sequence of \textit{small} moves would be equally hard as a unique move between two holds very far apart. The same observation can be done regarding the assymetry of a move since some moves can be technically so demanding that they will prevent any beginner climbers from going to the top of the route. Therefore, in order to predict efficiently the overall difficulty of a boulder, it would be relevant to try first to approximate these non linear functions associated with the distance and assymetry features. This is what is meant to implement this $1^{st}$ stage of the network.\\

\underline{NB} : After trying several variations of the neural layout of this stage, it was found more effective to feed conjointly the grade with the distance (resp. assymetry) feature.\\

\underline{$2^{nd}$ stage} : The second stage of the network, called \textit{Mixage}, is meant to assess the difficulty of the individual moves composing the boulder ie. to approximate the function $f_s$ describe at the end of the section \textbf{4.1}. Therefore, it was designed using a pyramidal layout in order to force the mixing of the chosen features that are fed as its inputs.\\

As mentionned in the previous sections, one of the main intricacies of the dataset was that each boulder has a variable-sized sequence of moves. This problem is addressed here by feeding succesively to these first two stages every move constituting the boulder. Therefore, they output a sequence $y_1, ~\!\cdots,~\! y_N$ that we hope will correctly represent the inherent difficulty of each move of the boulder.\\

\underline{$3^{rd}$ stage} : The final stage of the network is composed of the \textit{Demixage} block and a softmax activation function. It takes as an input the mean of the difficulty of the individual moves with hope that it will be a close approximation of the real difficulty of the boulder.

\[ d_{boulder}~=~\frac{1}{N_{move}} \sum_i ~ y_i \]

Due to the softmax activation function, this stage outputs a probability density over all the grade classes. We infer the predicted grade of the boulder by taking the label of the class having the maximum likelihood.\\

One last remark about the model of this classifier is that, after trying multiple variations of the inputs we feed it, it was found to be more effective to train separately the boulders with a steepness value of 40 and those having a steepness value of 20. Therefore, as depicted in the \figurename~\ref{switching_context}, the steepness value of a boulder serves as a \textit{switching-context} parameter that allows to separate the boulders in two sub-training sets depending on their steepness value. Each one of this two set will train their own network. Even if these two networks present the same architecture which was described above, this strategy allows to weight independently their neurons for more flexibility.\\


Therefore, the complete model is composed of two 3-stages networks in parallel and achieves our best accuracy score for the task of predicting a boulder grade. Indeed, it was found that keeping only one of the two networks to train the whole dataset without any consideration to the \textit{steepness context} was detrimental to the overall accuracy since the same sequence of moves ie. the same sequence of features ($\textit{dist}^{(i)},~\!\textit{grade}^{(i)},~\! \textit{assym}^{(i)}$) climbed on a $40^{\circ}$-wall is much harder than on a $25^{\circ}$-wall, resulting in adding some level of confusion during the training phase of the network. 

\begin{figure}[!h]
  \centering
  \includegraphics[width=\linewidth]{fig/switching_context.png}
  \caption{Schematisation of the \textit{switching-context} strategy.}
  \label{switching_context}
\end{figure}


\begin{figure*}
  \centering
  \includegraphics[width=.9\linewidth]{fig/archi_soft_max.png}
  \caption{Architecture of the shallow softmax classifier model.}
  \label{arch_soft_max}
\end{figure*}

\subsection{Training the models}
The proposed architectures were trained by trials and errors over several values of the training hyperparameters such as the number of epochs, the learning rate or the batch size. Since we could not apply techniques such as data augmentation in order to tackle the data imbalance issue, we attempted to solve this problem by weighting accordingly the loss functions. Therefore, for the simple \textit{Deep forward regression architecture}, we used the following weighted MSE loss :

\[ L(~\!boulder,~\!\theta~\!) ~=~ \omega^{(k)}(k-d^{\star})^2 \]

where $d^{\star}$ is the true difficulty of the boulder, $k$ is its predicted difficulty and $\omega^{(k)}$ is the weight associated with the predicted class. This weight is computed as the inverse of the frequency of the associated class in the dataset so that more attention is given to the less represented grades in the loss computation.\\

For the same reason, a weighted cross entropy loss is used for the training of the the \textit{Shallow softmax classifier architecture}.\\

As one can see in \figurename~\ref{quick_stop}, a problem we faced during the training phase of the models was to prevent it from overfitting the training set. Indeed, one can notice that the validation error, which represents the loss computed on the validation set, tends to increase with the number of epochs even if the training error keeps decreasing. Thus, after 20 epochs, the neural network starts to \textit{"learn by heart"} the features of the training set and will not generalize well to new data. In order to prevent this overfitting phenomenon, one strategy we applied was to stop the training as soon as it begins to overfit. In the next section, we refer to this strategy as \textit{quick stop}.

\begin{figure}[!h]
  \centering
  \includegraphics[width=\linewidth]{fig/advantage_quick_stop.png}
  \caption{Overfitting phenomenon for a training set containing 1000 boulders.}
  \label{quick_stop}
\end{figure}

\section{Evaluation}
The evaluation of our models is performed using a test set different
from the training set by extracting never seen boulders from the Moonboard 2017 dataset. The size
of this set is denoted $N_{\text{test}}$. We use a
strict metric of accuracy defined as follow:

\[ \text{Acc} = \frac{1}{N_{\text{test}}} \sum\limits_{n=1}^{N_{\text{test}}} \delta(\hat{y}_n -
  y_n) \]

Where $y_n$ stands for the true label (setter one) of the $n$-th boulder of the
test set, $\hat{y}_n$ stands for the predicted label of the $n$-th boulder and $\delta$ is the dirac function.\\

We evaluated seven different variations of the proposed architectures, each variation corresponding to a different tuning of the hyperparameters of the architecture such as its depth (number of layer per block) or the width of the layers as well as different training strategies. The results of these experiments are reported in \tablename~\ref{results}.\\

First, three different implementations of the deep forward
regression architecture were evaluated : one with 10 layers, one with 14 layers and one with 20
layers. These three models have been trained on a training set composed of 1000
boulders and have been tested on a test set of 2000 boulders.\\

Then, we evaluated the shallow softmax classifier
architecture using three different training strategies. The first one (shallow balanced context) was trained over a sampled subdataset extracted from the boulders with a 
steepness context parameter equals to 40, called Dataset40. As the training set was
sampled so that each class would be equally represented, it is limited by the number of boulder of the less represented class and therefore only contains a small number of boulders (75
boulders).\\

The second strategy was to train the model using the quick
stop scheme presented on the previous section in order to prevent overfitting on 1000 boulders extracted from the full Dataset40 (unbalanced classes).\\

The third strategy was to train the model over 1000 boulders without taking into account the \textit{steepness context} strategy  by sampling randomly boulders from the full Moonboard Dataset. One last variation was to only consider the 8 most common classes when training and evaluating the model. This case is not relevant in practice but can serve as a baseline of what are the best performances we can expect from this model in a case of the balanced dataset.\\

\begin{figure*}
  \centering
  \subcaptionbox{Precision of the predicted grades. The red dashed line\\ represents the target predictions. The blue errobars repre-\\sent the standard variation of the prediction within each\\ class.}{\includegraphics[width=.45\linewidth]{fig/precision.png}}
  \subcaptionbox{Comparaison of the distribution of the predicted grades and the
  real grades.}{\vspace*{2.5mm}\includegraphics[width=.45\linewidth]{fig/distribution_comp.png}}
  \caption{Precision of the prediction. We observe that our model manages to
    approximate the grades distribution but struggles to predict difficult
    boulders as it tends to over-predicted the most commom classes.}
  \label{precision}
\end{figure*}

\begin{table}
  \centering
  \begin{tabular}{cccc}
    \hline
    \hline
    Model & Epoch & Accuracy & Loss \\
    \hline
    \hline
    Random 8 classes & - & 12.5 \% & - \\
    Random 17 classes & - & 5.9 \% & - \\
    \hline
    Deep forward 10 layers & 100 & 20.83 \% & 3.1741 \\
    Deep forward 14 layers & 100 & 21.12 \% & 3.27 \\
    Deep forward 20 layers & 100 & 13.89 \% & 6.2\\
    \hline
    Shallow balanced context  & 100 & 14.56 \% & 1.8505 \\
    Shallow QS full context& 20 & \textbf{34.12} \% & 1.6246 \\
    Shallow full no-context& 1000 & 29.39 \% & 1.6922 \\
    Shallow 8 grades Only & 1000 & \textbf{39.81} \% & - \\
    \hline
    \hline
  \end{tabular}
  \caption{Summary of the experiments results with number of epochs, accuracy
    over the test set and the weighted cross entropy loss reached at the end of the
    training phase.}
  \label{results}
\end{table}

As shown in \tablename~\ref{results}, our best model achieves an
accuracy of 34.12 \% over all the 17 grades. This result is reached by the
shallow softmax architecture when training it during 20 epochs using the quick stop (QS) scheme. On the contrary, according to the results presented in \tablename~\ref{results}, the \textit{benchmark} deep forward
regression architecture performs quite poorly. Moreover, it has been quite
surprising to observe that going deeper does not necessarly give
better results. \\

We can remark that the shallow model trained over a balanced distribution of the
grades also performs poorly. One reason that may account for this poor accuracy may be
that the training set is not large enough to enable the model to learn anything. This
outlines that using an upsampling / downsampling
strategy to overcome the problem arising from the imbalanced class distribution does not work in this application.\\


Moreover, we can
observe that the shallow architecture produces far better performance than the
deep forward one. This result confirms our a priori about the construction of
the shallow softmax classifier architecture as described in the associated section.\\

Another interesting result is that,
when training this model only over the most common grade classes, it is giving excellent performance in comparaison to the current state-of-the-art strategies
(39.81 \% accuracy) which confirms that our method is relevant for classifying
climbing routes but struggles to predict hard routes because of their lack of
representativity in the Moonboard dataset.\\


Finally, as shown in \figurename~\ref{results}, our model manages to predict the true difficulty of a boulder with an average error of +/- one grade which means that the low accuracy observed is due to its very strict definition.




\section{Conclusion}

To conclude this project, we can say that we achieved to propose an original
approach to the rocks climbing routes classification problem.  The selected features have proven to be relevant to represent well the difficulty of a
boulder and our model
performed almost state-of-the-art accuracy: our model performed 34.12 \% of
accuracy whereas the best state of the art performances lie whithin the range of 35-40 \% top accuracy
\cite{stanfordClimb}. However, the clear advantage of our approach is its simplicity and its very low computational cost due to the drastically reduced dimension of the input space. Therefore it is able to be easily trained on any CPU - the described shallow softmax classifier network has a very low number of neurons (approximately 200) compared to the complex and very deep architectures proposed on the literature (with up to 50 000 independent parameters).\\

In the future, it would be interesting to understand why we struggle to go beyond 
an accuracy of 35 \%. One guess is that the dataset itself makes the training of
the network quite hard due to its inherent imbalanced distribution of the class. Another possible explanation that may account for this low accuracy may be that the grading of climbing
routes is a quite subjective task. For example, the assigned grade of a boulder always remains open to discussion within the climbing
community. Therefore, maybe that the neural network struggles to predict the exact difficulty because the dataset contains many misgraded boulders implying that there is no exact patterns within the data for grade assessment. 



{\small
\bibliographystyle{ieee_fullname}
\bibliography{biblio}
}

\end{document}