Classification of bouldering difficulty
=

  This work has been carried out as a final project of the Deep Learning course of
  the MVA Master at ENS Paris Saclay. In this work, we propose an original approach
  to the classification problem of rock climbing routes.
  
  Our approach enables to
  almost reach state of the art accuracy using a simple shallow neural
  network that can easily be trained on any computer CPU. To tackle this
  classification problem, we first proposed to reduce the dimension of the input space by representing each boulder as a sequence of a small amount of features that were considered discriminative for assessing its difficulty.  Then, we designed two types of
  deep learning model, based on simple dense neural
  network, that takes as input these chosen features and output the predicted
  difficulty of the boulder as a grade. 
  
  The training and the evaluation of our models have been performed on the
  moonboard 2017 dataset.
  
  To discover more about our project, read our [report](report/main.pdf) !

The architecture designed
-

We proposed the following architecture for predictong grades of boulders routes.

![](report/fig/archi_soft_max.png)

Some results
-
![](report/fig/precision.png)
![](report/fig/distribution_comp.png)

Contributions
-

This project has been realised by Thomas Chapalain and Nicolas Atienza for the final project of the deep learning class.
